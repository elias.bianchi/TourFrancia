package com.example.tourFrancia.Dtos;

import com.example.tourFrancia.domain.Country;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class CyclingTeamDto {
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotNull
    private Country country;

    @JsonIgnore
    private String teamCode;

    public CyclingTeamDto(String name, Country country) {
        this.name = name;
        this.country = country;
        this.teamCode = UUID.randomUUID().toString().substring(0, 3);
    }
}
