package com.example.tourFrancia.Dtos;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CyclistDto {
    @NotBlank(message = "Name is mandatory")
    private String fullname;
    @NotBlank(message = "CompetitorNumber is mandatory")
    private String competitorNumber;

}
