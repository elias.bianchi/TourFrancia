package com.example.tourFrancia.Dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.UUID;

@Data
public class CountryDto {
    @NotBlank(message = "Name is mandatory")
    private final String name;
    @JsonIgnore
    private String code;

    @JsonCreator
    public CountryDto(@JsonProperty("name") String name) {
        super();
        this.name = name;
        this.code = UUID.randomUUID().toString().substring(0, 3);
    }
}
