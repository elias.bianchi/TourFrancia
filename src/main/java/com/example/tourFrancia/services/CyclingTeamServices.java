package com.example.tourFrancia.services;

import com.example.tourFrancia.domain.CyclingTeam;
import com.example.tourFrancia.repository.CyclingTeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class CyclingTeamServices {
    @Autowired
    CyclingTeamRepository repository;

    public Flux<CyclingTeam> getAllCyclingTeams() {
        return  repository.findAll();
    }

    public Mono<CyclingTeam> getCyclingTeamByTeamCode(String code) {
        return repository.findById(code);
    }

    public Mono<CyclingTeam> getTeamByCountryCode(String countryCode) {return repository.findByCountry_Code(countryCode);}
    public Mono<CyclingTeam> insertCyclingTeam(CyclingTeam cyclingTeam){
        return repository.insert(cyclingTeam);
    }

    public Mono<CyclingTeam> updateCyclingTeam(CyclingTeam cyclingTeam, String id) {
        return repository.findById(id)
                .flatMap(cyclingTeam1 -> {
                    cyclingTeam1.setName(cyclingTeam.getName());
                    cyclingTeam1.setCountry(cyclingTeam.getCountry());
                    cyclingTeam1.setTeamCode(cyclingTeam.getTeamCode());
                    return repository.save(cyclingTeam1);
                })
                .switchIfEmpty(Objects.requireNonNull(repository.save(cyclingTeam)));
    }

    public Mono<Void> removeCyclingTeam(String id) {
        return repository.deleteById(id);
    }
}
