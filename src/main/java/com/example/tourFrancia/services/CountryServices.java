package com.example.tourFrancia.services;

import com.example.tourFrancia.domain.Country;
import com.example.tourFrancia.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class CountryServices {
    @Autowired
    private CountryRepository repository;

    public Flux<Country> getAllCountries(){
        return repository.findAll();
    }

    public Mono<Country> getCountryByCode(String code) {
        //        Optional<Country> country = repository.findCountryByCode(code);
        //        if (country.isPresent()) {
        //            return ResponseEntity.ok().body(country.get());
        //        } else {
        //            return ResponseEntity.notFound().build();
        //        }
        return repository.findById(code);
    }

    public Mono<Country> insertCountry(Country country){
        return repository.insert(country);
    }

    public Mono<Country> updateCountry (Country newCountry, String id) {
        //        return repository.findById(id).map(country -> {
        //                    country.setName(newCountry.getName());
        //                    country.setCode(newCountry.getCode());
        //                    return repository.save(country);
        //                })
        //                .orElseGet(() -> {
        //                    return repository.save(newCountry);
        //                });
        return repository.findById(id)
                .flatMap(country -> {
                    country.setName(newCountry.getName());
                    country.setCode(newCountry.getCode());
                    return repository.save(country);
                })
                .switchIfEmpty(Objects.requireNonNull(repository.save(newCountry)));
    }

    public Mono<Void> removeCountry(String id) {
        //        if (repository.existsById(id))
        //            repository.deleteById(id);
        //        else
        //            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        //
        //        return new ResponseEntity<Void>(HttpStatus.OK);
        return repository.deleteById(id);
    }
}
