package com.example.tourFrancia.services;

import com.example.tourFrancia.domain.Cyclist;
import com.example.tourFrancia.repository.CyclistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class CyclistServices {
    @Autowired
    CyclistRepository repository;

    public Flux<Cyclist> getAllCyclistTeams() {
        return repository.findAll();
    }

    public Mono<Cyclist> getCyclistByCompetitorNumber(String competitorNumber) {
        return repository.findByCompetitorNumber(competitorNumber);
    }

    public Flux<Cyclist> getCyclistByTeamCode(String teamCode) {
        return repository.findAllByCyclingTeamTeamCode(teamCode);
    }

    public Flux<Cyclist> getCyclistsByCountryCode(String countryCode) {
        return repository.findAllByCountryCode(countryCode);
    }

    public Mono<Cyclist> insertCyclingTeam(Cyclist cyclingTeam){
        return repository.insert(cyclingTeam);
    }

    public Mono<Cyclist> updateCyclist(Cyclist cyclist, String id) {
        return repository.findById(id)
                .flatMap(cyclist1 -> {
                    cyclist1.setFullname(cyclist.getFullname());
                    cyclist1.setCompetitorNumber(cyclist.getCompetitorNumber());
                    cyclist1.setCountry(cyclist.getCountry());
                    cyclist1.setCyclingTeam(cyclist.getCyclingTeam());
                    return repository.save(cyclist1);
                })
                .switchIfEmpty(Objects.requireNonNull(repository.save(cyclist)));
    }

    public Mono<Void> removeCyclist(String id) {
        return repository.deleteById(id);
    }
}
