package com.example.tourFrancia.domain;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Document(collection = "teams")
@Data
public class CyclingTeam {
    @Id
    private ObjectId id;
    private String name;
    private String teamCode;

    @DocumentReference(lazy = true)
    private Country country;

    @DocumentReference(lazy = true)
    private List<Cyclist> cyclists;

}
