package com.example.tourFrancia.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Document(collection = "countries")
@NoArgsConstructor
@Data
public class Country {
    @Id
    private ObjectId id;
    private String name;
    private String code;

    @DocumentReference(lazy = true)
    private List<CyclingTeam> cyclingTeams;
    @DocumentReference(lazy = true)
    private List<Cyclist> cyclists;
}
