package com.example.tourFrancia.domain;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

@Document(collection = "cyclists")
@NoArgsConstructor
@Data
public class Cyclist {
    @Id
    private ObjectId id;
    private String fullname;
    private String competitorNumber;

    @DocumentReference(lazy = true)
    private Country country;
    @DocumentReference(lazy = true)
    private CyclingTeam cyclingTeam;
}
