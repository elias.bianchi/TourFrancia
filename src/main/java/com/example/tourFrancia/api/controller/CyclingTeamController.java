package com.example.tourFrancia.api.controller;

import com.example.tourFrancia.AppUtils;
import com.example.tourFrancia.Dtos.CyclingTeamDto;
import com.example.tourFrancia.domain.CyclingTeam;
import com.example.tourFrancia.services.CyclingTeamServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/cyclingTeam")
public class CyclingTeamController {
    @Autowired
    private CyclingTeamServices services;

    @GetMapping()
    public Flux<CyclingTeam> findAllCyclingTeams() {
        return services.getAllCyclingTeams();
    }

    @PostMapping("/newTeam")
    public Mono<CyclingTeam> saveCyclingTeam(@Validated @RequestBody CyclingTeamDto newTeamDto) {
        return services.insertCyclingTeam(AppUtils.dtoToCyclingTeam(newTeamDto));
    }

    @GetMapping("/{team_code}")
    public Mono<CyclingTeam> findCyclingTeamByCode(@PathVariable(name = "team_code") String teamCode) {
        return services.getCyclingTeamByTeamCode(teamCode);
    }

    @GetMapping("/country/{countrycode}")
    public Mono<CyclingTeam> findTeamByCountry(@PathVariable(name="countrycode") String countryCode){
        return services.getTeamByCountryCode(countryCode);
    }

    @PutMapping("/{id}")
    public Mono<CyclingTeam> upgradeCyclingTeam(@Validated @RequestBody CyclingTeamDto cyclingTeamDto, @PathVariable String id){
        return services.updateCyclingTeam(AppUtils.dtoToCyclingTeam(cyclingTeamDto), id);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteCyclingTeam(@PathVariable String id) {
        return services.removeCyclingTeam(id);
    }
}
