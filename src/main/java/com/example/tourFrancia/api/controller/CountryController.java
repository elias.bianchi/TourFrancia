package com.example.tourFrancia.api.controller;

import com.example.tourFrancia.AppUtils;
import com.example.tourFrancia.Dtos.CountryDto;
import com.example.tourFrancia.domain.Country;
import com.example.tourFrancia.services.CountryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/api/countries")
public class CountryController {

    @Autowired
    public CountryServices service;

    @GetMapping
    public Flux<Country> findAllCountries() {
        return service.getAllCountries();
    }

    @GetMapping("/{code}")
    public Mono<Country> findCountryByCode(@PathVariable(name = "code") String code) {
        return service.getCountryByCode(code);
    }

    @PostMapping("/newCountry")
    public Mono<Country> saveCountry(@Validated @RequestBody CountryDto countryDto) {
        return service.insertCountry(AppUtils.dtoToCountry(countryDto));
    }

    @PutMapping("/{id}")
    public Mono<Country> updateCountry(@RequestBody CountryDto newCountryDto, @PathVariable String id) {
        return service.updateCountry(AppUtils.dtoToCountry(newCountryDto), id);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteCountry(@PathVariable String id) {
        return service.removeCountry(id);
    }
}
