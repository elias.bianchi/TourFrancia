package com.example.tourFrancia.api.controller;

import com.example.tourFrancia.AppUtils;
import com.example.tourFrancia.Dtos.CyclistDto;
import com.example.tourFrancia.domain.Cyclist;
import com.example.tourFrancia.services.CyclistServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/api/cyclists")
public class CyclistController {
    @Autowired
    public CyclistServices service;

    @GetMapping
    public Flux<Cyclist> findAllCyclists(@RequestParam Map<String, String> reqParam) {
        return service.getAllCyclistTeams();
    }

    @PostMapping("/newCyclist")
    public Mono<Cyclist> saveNewCyclist(@Validated @RequestBody CyclistDto newCyclistDto) {
        return service.insertCyclingTeam(AppUtils.dtoToCyclist(newCyclistDto));
    }

    @GetMapping("/{competitorNumber}")
    public Mono<Cyclist> findCyclistByCompetitorNumber(@PathVariable(name = "competitorNumber") String competitorNumber) {
        return service.getCyclistByCompetitorNumber(competitorNumber);
    }

    @RequestMapping(value = "/team", method = RequestMethod.GET, params = "teamCode")
    public Flux<Cyclist> findCyclistsByTeamCode(@RequestParam(name = "teamCode") String teamCode) {
        return service.getCyclistByTeamCode(teamCode);
    }

    @RequestMapping(value = "/country/", method = RequestMethod.GET, params = "countryCode")
    public Flux<Cyclist> findCyclistsByCountryCode(@RequestParam(name = "countryCode") String countryCode) {
        return service.getCyclistsByCountryCode(countryCode);
    }
    @GetMapping("/country/{countryCode}")
    public Flux<Cyclist> findCyclistsByCountry (@PathVariable(name = "countryCode") String countryCode) {
        return service.getCyclistsByCountryCode(countryCode);
    }

    @PutMapping("/{id}")
    public Mono<Cyclist> updateCyclist(@RequestBody CyclistDto cyclistDto, @PathVariable String id) {
        return service.updateCyclist(AppUtils.dtoToCyclist(cyclistDto), id);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteCyclist(@PathVariable String id) {
        return service.removeCyclist(id);
    }
}
