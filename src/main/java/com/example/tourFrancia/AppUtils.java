package com.example.tourFrancia;

import com.example.tourFrancia.Dtos.CountryDto;
import com.example.tourFrancia.Dtos.CyclingTeamDto;
import com.example.tourFrancia.Dtos.CyclistDto;
import com.example.tourFrancia.domain.Country;
import com.example.tourFrancia.domain.CyclingTeam;
import com.example.tourFrancia.domain.Cyclist;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;

public class AppUtils {

    static public CyclingTeam dtoToCyclingTeam(CyclingTeamDto dto) {
        CyclingTeam team = new CyclingTeam();
        BeanUtils.copyProperties(dto, team);
        return team;
    }
    static public Cyclist dtoToCyclist(CyclistDto dto) {
        Cyclist cyclist = new Cyclist();
        BeanUtils.copyProperties(dto, cyclist);
        return cyclist;
    }
    static public Country dtoToCountry(CountryDto dto) {
        Country country = new Country();
        BeanUtils.copyProperties(dto, country);
        return country;
    }
}
