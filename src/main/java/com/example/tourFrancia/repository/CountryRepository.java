package com.example.tourFrancia.repository;

import com.example.tourFrancia.domain.Country;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryRepository extends ReactiveMongoRepository<Country, String> {
    Optional<Country> findCountryByCode(String code);
}
