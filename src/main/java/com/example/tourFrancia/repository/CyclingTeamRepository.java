package com.example.tourFrancia.repository;

import com.example.tourFrancia.domain.CyclingTeam;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

public interface CyclingTeamRepository extends ReactiveMongoRepository<CyclingTeam, String> {
    public Mono<CyclingTeam> findByCountry_Code(String teamCode);
}
