package com.example.tourFrancia.repository;

import com.example.tourFrancia.domain.Cyclist;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CyclistRepository extends ReactiveMongoRepository<Cyclist, String> {

    public Mono<Cyclist> findByCompetitorNumber(String teamCode);

    public Flux<Cyclist> findAllByCyclingTeamTeamCode(String teamCode);

    public Flux<Cyclist> findAllByCountryCode(String teamCode);


}
